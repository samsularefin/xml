#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ticket.h"
#include <packedobjects/packedobjects.h>

#define XML_SCHEMA "ticket.xsd"
#define MAXCHARS 256

#define HOST_IP "127.0.0.1"
#define HOST_PORT 6969


int main(int argc, char **argv)
{
  xmlDocPtr doc = NULL;
  packedobjectsContext *pc = NULL;
  char *pdu = NULL;
  ssize_t bytes_sent;
  int       sock;                                                 
  struct    sockaddr_in servaddr;

  // setup socket
  if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
    fprintf(stderr, "Error creating socket.\n");
    exit(EXIT_FAILURE);
  }
  // setup addressing
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family      = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(HOST_IP);
  servaddr.sin_port        = htons(HOST_PORT); 
  
  // initialise packedobjects
  if ((pc = init_packedobjects(XML_SCHEMA, 0, 0)) == NULL) {
    printf("failed to initialise libpackedobjects");
    exit(1);
  }
  
  // create the data
  doc = xmlNewDoc(BAD_CAST "1.0");
  add_ticket_data(doc);
  
  ////////////////////// Encoding //////////////////////
  
  
  // encode the XML DOM
  pdu = packedobjects_encode(pc, doc);
  if (pc->bytes == -1) {
    printf("Failed to encode with error %d.\n", pc->encode_error);
  }


  // make network connection
  if (connect(sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
    fprintf(stderr, "Error calling connect()\n");
    exit(EXIT_FAILURE);
  }
  
  // send the pdu across the network
  bytes_sent = send(sock, pdu, pc->bytes, 0);
  printf("Bytes sent : %d\n",(int)bytes_sent);
  if (bytes_sent != pc->bytes) {
    fprintf(stderr, "Error calling send()\n");
    exit(EXIT_FAILURE);
  }
  
  if ( close(sock) < 0 ) {
    fprintf(stderr, "Error calling close()\n");
    exit(EXIT_FAILURE);
  }
  
  // output the DOM for checking
  packedobjects_dump_doc(doc);
  xmlSaveFormatFileEnc("ticke.xml",doc,"UTF-8",1);
  xmlFreeDoc(doc);
  
  // free memory created by packedobjects
  free_packedobjects(pc);
  
  xmlCleanupParser();
  return(0);
}

