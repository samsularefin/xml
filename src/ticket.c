#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <packedobjects/packedobjects.h>
#include <errno.h>
#include "ticket.h"

#define XML_SCHEMA "ticket.xsd"

#define MAXCHARS 256


int add_ticket_data(xmlDocPtr doc)
{
  xmlNodePtr root_node = NULL,node= NULL,node_t = NULL,node_p = NULL,  node_times = NULL, node_prices = NULL;// assigning the child nodes
  char buffer[MAXCHARS];
  char buffer2[MAXCHARS];
  char buffer3[MAXCHARS];
  char buffer4[MAXCHARS];
  char buffer5[MAXCHARS];
  char*end = NULL;
  int p_count = 0;
  int loop = 0;
  float b;
  errno =0;
  
  time_t rawtime;
  struct tm*  time_;
  
  printf ("\n\n\t TRAIN TIMES & TICKETS  \n\n");
  
  
  time(&rawtime);
  time_ = localtime(&rawtime);//Generates current time
  
  printf("Current time is %d:%d\n",time_->tm_hour, time_->tm_min);
  printf("Todays date is %d/%d/%d\n",time_->tm_mday, time_->tm_mon+1,time_->tm_year+1900);//these are built in pointers in time_
  
  root_node = xmlNewNode(NULL, BAD_CAST "tickets");
  xmlDocSetRootElement(doc, root_node);
  
  
  printf("How many tickets need to be processed: "); // 
  if (fgets(buffer3, MAXCHARS, stdin) == NULL) {
    fprintf(stderr, "No valid Input \n");
    return (EXIT_FAILURE);// stores the user input in buffer 3
  }
  
  strtok(buffer3, "\n");// removing new line character
  loop = strtoul(buffer3, &end, 10);// convertion to unsigned int.
  if (errno !=0){
    
    fprintf(stderr, "Convertion Failed %s\n", strerror(errno));
    exit (EXIT_FAILURE);
  }else if (*end){
    
    printf("Alert: Part converdet: %i, non-covertible: %s\n", p_count, end);
    
  }
  
  for (p_count =0 ; p_count < loop; p_count++) {
    //   clears our buffers each time
    bzero(buffer, MAXCHARS);
    bzero(buffer2,MAXCHARS);
    bzero(buffer4, MAXCHARS);
    bzero(buffer5,MAXCHARS);
    
    node = xmlNewChild(root_node, NULL, BAD_CAST "ticket", NULL);
    printf(" From: ");
    if (fgets(buffer, MAXCHARS, stdin) == NULL) {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);//Gets user input for diparture and stores in buffer
    }
    strtok(buffer, "\n");
    
    printf(" To: ");
    if (fgets(buffer2, MAXCHARS, stdin) == NULL) {
      fprintf(stderr, "failed to read string\n");
      return (EXIT_FAILURE);//Gets user input for destination and stores in buffer2
    }
    strtok(buffer2, "\n");
    
    srand ((unsigned int) time(NULL));//gets a random float number
    b =((float)rand())/(float)(RAND_MAX);// limits the float range
    sprintf(buffer4, "%.2f",b*30);// sets highest float limit to 30
    sprintf(buffer5, "%.2f",(b*30)+5);// adds 5 more to previous value and stre in separate buffer
    
    
    xmlNewChild(node , NULL , BAD_CAST "start", BAD_CAST buffer);//assigns the value for deparing point
    xmlNewChild(node , NULL , BAD_CAST "dest", BAD_CAST buffer2);//assigns value for destination point
    
    node_t = xmlNewChild(node, NULL, BAD_CAST "times", NULL);
    
    node_times = xmlNewChild(node_t, NULL, BAD_CAST "time", NULL);
    xmlNewChild(node_times, NULL, BAD_CAST "depart", BAD_CAST "6.30");
    xmlNewChild(node_times, NULL, BAD_CAST "arrival", BAD_CAST "13.30");
    
    
    node_times = xmlNewChild(node_t, NULL, BAD_CAST "time", NULL);
    xmlNewChild(node_times, NULL, BAD_CAST "depart", BAD_CAST "12.30");
    xmlNewChild(node_times, NULL, BAD_CAST "arrival", BAD_CAST "19.30");
    
    node_times = xmlNewChild(node_t, NULL, BAD_CAST "time", NULL);
    xmlNewChild(node_times, NULL, BAD_CAST "depart", BAD_CAST "16.30");
    xmlNewChild(node_times, NULL, BAD_CAST "arrival", BAD_CAST "23.30");
    
    node_p= xmlNewChild(node, NULL, BAD_CAST "prices", NULL);
    
    node_prices= xmlNewChild(node_p, NULL, BAD_CAST "price", NULL);
    
    xmlNewChild(node_prices, NULL, BAD_CAST "class1", BAD_CAST buffer5);//gets the price for 1st class from buffer5
    xmlNewChild(node_prices, NULL, BAD_CAST "economy", BAD_CAST buffer4);//gets economy class price from buffer4
    
    
    
    
  }
  return (EXIT_SUCCESS);
}
